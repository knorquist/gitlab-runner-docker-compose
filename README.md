# gitlab-runner-docker-compose.yml

This is a Docker Compose file for GitLab Runner.

## Usage

- Copy the sample.env to .env
- Edit .env to match runner name and registration_token
- Run `docker compose up -d`

This is a fork of [@Tylsl's](https://gitlab.com/Tylsl) Docker Compose template for creating a GitLab Runner, found [here](https://gitlab.com/TyIsI/gitlab-runner-docker-compose).

## Notes

From [@Tylsl's](https://gitlab.com/Tylsl) notes in his [forum post](https://forum.gitlab.com/t/example-gitlab-runner-docker-compose-configuration/67344):

* Networks are not specified so Docker Compose creates a project network defaulting to bridge mode.
* Leaving `DOCKER_TLS_CERTDIR` empty forces the `dind` instance to use plain TCP.
* The runner connects to `dind` over TCP.
* The `docker.sock` comment references the `dind` executor, which is how the containers in the Docker container talk to Docker.
